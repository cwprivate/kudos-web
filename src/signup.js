import React, { Component, useEffect } from 'react';
import { fetchMiddleware } from './utils/fetch'

export default class SignupForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { name: 'nisheetsun@gmail.com', 'password': 'sss', 'loading': true, 'companies': '', 'email': '', 'selectedCompany': '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.getCompanies()
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    getCompanies() {
        fetchMiddleware('employee/get-companies/', 'GET', { 'Content-Type': 'application/json' }, (response) => { this.setState({ 'companies': response['list'] }) })
    }

    handleSubmit(event) {
        alert(this.state.selectedCompany !== '')
        if (this.state.selectedCompany) {
            fetchMiddleware('auth/signup/', 'POST', { 'Content-Type': 'application/json' }, (response) => {}, JSON.stringify({ 'email': this.state.name, 'password': this.state.password, 'company_id': this.state.selectedCompany }))
        }
        event.preventDefault();
    }


    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                  name:
                  <input placeholder="enter name" type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                </label>
                <br/>
                <label>
                  password:
                  <input placeholder="enter password" type="password" name="password" value={this.state.password} onChange={this.handleChange} />
                </label>
                <br/>
                <label>
                  email:
                  <input placeholder="enter email" type="email" name="email" value={this.state.email} onChange={this.handleChange} />
                </label>
                <br/>
                {
                  this.state.companies
                    ?
                    <select name="selectedCompany" value={this.state.selectedCompany} onChange={this.handleChange}>
                        {
                              this.state.companies.map((comapany)=>{
                                return <option key={comapany.name} value={comapany.id}>{comapany.name}</option>
                            })
                        }
                        { <option selected value={''} >SELECT</option> }
                        </select>
                        :
                        'loading'
                }
              <br/>
              <input type="submit" value="Submit" />
          </form>

        );
    }
}