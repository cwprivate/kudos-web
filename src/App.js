import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import Loader from 'react-loader-spinner'

import LoginForm from './login'
import SignupForm from './signup'
import Dash from './dashboard'


export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loading: true, loggedIn: false, token: null };
        this.changeIsLoggedIn = this.changeIsLoggedIn.bind(this);
    }

    componentDidMount() {
        let token = localStorage.getItem("token");
        let loggedInAs = localStorage.getItem("loggedInAs");
        if (token) {
            this.setState({
                loggedIn: true,
                token: token,
                loggedInAs: loggedInAs
            });
        }
    }

    getLoader() {
        return <Loader
         type="Puff"
         color="#00BFFF"
         height={100}
         width={100} />
    }

    Routes() {
        return (
            <Switch>
            <Route exact path="/">
              <div/>
            </Route>
            <Route path="/login">
                <LoginForm changeIsLoggedIn={this.changeIsLoggedIn}/>
            </Route>
            <Route path="/signup">
                <SignupForm/>
            </Route>
            <Route path="/dashboard">
                <Dash token={this.state.token}/>
            </Route>
            
        </Switch>
        )
    }

    changeIsLoggedIn(value, name, token) {
        try {
            this.setState({ loggedIn: value, loggedInAs: name, token: token })
        } catch (e) {
            alert(e)
        }
    }

    render() {
        return (
            <div>
        <div>
          {
            this.state.loggedIn?
            <div>
              <button
              style={{color:'red'}} onClick={
                  ()=>{
                    localStorage.removeItem("token")
                    this.changeIsLoggedIn(false)
                  }
              }>
                  LOGOUT
              </button>
              <br/>
              LOGGED IN AS:  {this.state.loggedInAs}
              <br/><br/>
            <hr/>
            </div>
            :
            null
          }
        </div>
        <Router>
          {this.Routes()}
          {this.state.loggedIn ? <Redirect to="/dashboard" /> : <Redirect to="/login" />}
        </Router>
      </div>
        );
    }
}