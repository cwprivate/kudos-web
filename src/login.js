import React from 'react';
import {
    Link
} from "react-router-dom";

import { fetchMiddleware } from './utils/fetch'


export default class NameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { username: 'test@gmail.com', 'password': 'test' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        fetchMiddleware('auth/login/', 'POST', { 'Content-Type': 'application/json' }, (response) => {
            if (response['valid']) {
                localStorage.setItem("token", response['data']['token']);
                localStorage.setItem("loggedInAs", response['data']['email']);
                this.props.changeIsLoggedIn(true, response['data']['email'], response['data']['token'])
            }else{
                alert('wrong credentials')
            }
        }, JSON.stringify({ 'email': this.state.username, 'password': this.state.password }))
        event.preventDefault();
    }


    render() {
        return (
            <div>
              <form onSubmit={this.handleSubmit}>
                <label>
                  username:
                  <input placeholder="enter username" type="text" name="username" value={this.state.username} onChange={this.handleChange} />
                </label>
                <br/>
                <label>
                  password:
                  <input placeholder="enter password" type="password" name="password" value={this.state.password} onChange={this.handleChange} />
                </label>
                <br/>
                <input type="submit" value="Submit" />
              </form>
              <Link to="/signup">sign up</Link>
          </div>
        );
    }
}