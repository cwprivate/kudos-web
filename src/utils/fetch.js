// import { HOME_URL } from './variables'
var HOME_URL = 'http://127.0.0.1:8000/'
export function fetchMiddleware(url, method, headers, callback, body) {
	if (body){
		fetch(HOME_URL+url, { 'method': method, 'headers': headers, 'body': body })
    .then(response => response.json())
    .then(response=>{callback(response)})
	}else{
		fetch(HOME_URL+url, { 'method': method, 'headers': headers })
    .then(response => response.json())
    .then(response=>{callback(response)})
	}
}

// export function fetchMiddleware(){
// 	alert('ss')
// }