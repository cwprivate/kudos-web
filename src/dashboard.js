import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import { fetchMiddleware } from './utils/fetch'


class EmployeeCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = { message: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        fetchMiddleware('kudos/', 'POST', { 'Content-Type': 'application/json', 'Authorization': this.props.token }, (response) => {
            if (response['valid']) {
                alert('kudos successful')
                this.setState({ message: '' })
            } else {
                alert(response['message'])
            }
        }, JSON.stringify({ 'to_employee': this.props.data.id, 'message': this.state.message }))
        event.preventDefault();
    }

    render() {
        return (
            <div>
              <hr/>
                <h4>Name:  {this.props.data.name}</h4>
                <div>Thumbnail:  </div>
                <img src={this.props.data.thumbnail===''?'https://upload.wikimedia.org/wikipedia/en/2/2e/In_Rainbows_Official_Cover.jpg':this.props.data.thumbnail}  width="70" height="70" />     
                <br/>
                <br/>
                <form onSubmit={this.handleSubmit}>
                <label>
                  Message:
                  <input type="text" name="message" value={this.state.message} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Give Kudos" />
              </form>          
              <hr/>
            </div>
        )
    }
}


export default class Dash extends React.Component {
    constructor(props) {
        super(props);
        this.state = { employees: [], myKudos: [], message: {}, selectedIndex: 0 };
        this.getAllEmployees = this.getAllEmployees.bind(this);
    }

    componentDidMount() {
        this.getAllEmployees()
    }

    getAllEmployees() {
        if (this.props.token) {
            fetchMiddleware('employee/get-employees/', 'GET', {
                'Content-Type': 'application/json',
                'Authorization': this.props.token
            }, (response) => { this.setState({ employees: response['data']['list'] }) }, null)
        }
    }

    myKudosCard(key) {
        return (
            <div key={key['message']+key['from']}>
              <hr/>
                KUDOS FROM: {key['from']}
                <br/>
                MESSAGE: {key['message']}
              <hr/>
            </div>
        )

    }

    getMyKudos() {
        if (this.props.token) {
            fetchMiddleware('kudos/', 'GET', {
                'Content-Type': 'application/json',
                'Authorization': this.props.token
            }, (response) => { this.setState({ myKudos: response['data']['list'] }) }, null)
        }
    }

    render() {
        return (
            <div>
              <h1>Dasboard PAGE</h1>
                <Tabs onSelect={index => {
                  this.setState({selectedIndex:index})
                  if(index === 0){
                    this.getAllEmployees()
                  }else{
                    this.getMyKudos()
                  }
                }}>
                  <br/>
                  <div>Select following Tab to Activate</div>
                  <TabList>
                    <Tab><h3 style={{'backgroundColor':this.state.selectedIndex===0?'red':''}}>Give kudos TAB</h3></Tab>
                    <Tab><h3 style={{'backgroundColor':this.state.selectedIndex===1?'red':''}}>My kudos TAB</h3></Tab>
                  </TabList>
               
                  <TabPanel>
                    <hr/>
                    <div>
                      <h2>SELECTED TAB: Give kudos</h2>
                      {this.state.employees.map((key)=>{
                        return (<EmployeeCard token={this.props.token} data={key} key={key.name} />)
                      })}
                    </div>
                    <div>
                  </div>
                  </TabPanel>
                  <TabPanel>
                    <hr/>
                    <div>
                      <h2>SELECTED TAB: My kudos</h2>
                      {this.state.myKudos.map((key)=>{
                        return (this.myKudosCard(key))
                      })}
                    </div>
                  </TabPanel>
                </Tabs>
              </div>
        );
    }
}