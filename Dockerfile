FROM node:8 as react-build
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
CMD ["npm", "start"]