import React, { Component, useEffect, useState } from 'react';
import Loader from 'react-loader-spinner'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
  Redirect,
} from "react-router-dom";


import LoginForm from './login'
import SignupForm from './signup'
import logo from './logo.svg';
import './App.css';


const Routes = () => {
  const [loggedIn, setloggedIn] = useState(false);
    useEffect(() => {
    let token = localStorage.getItem("token");
    if (token){
      setloggedIn(true)
    }
  }, [])
    alert(loggedIn)
  return (
    <Switch>
      <Route exact path="/">
        {/*LoaderFunction()*/}
         {loggedIn ? <Redirect to="/dashboard" /> : <Redirect to="/login" />}
        <div/>
      </Route>
      <Route path="/login">
        <div>ddddd</div>
        <LoginForm/>
      </Route>
      <Route path="/signup">
        <SignupForm/>
      </Route>
      <Route path="/dashboard">
        <div />
      </Route>
      <Route path="/loader">
        {LoaderFunction()}
      </Route>
    </Switch>)
}

  // let history = useHistory();
  // useEffect(() => {
  //   let token = localStorage.getItem("token");
  //   if (token){
  //     alert("!!")
  //     history.push("/dashboard");
  //   }else{
  //     history.push("/login");
  //   }
  // }, [])

function LoaderFunction() {
  // alert('dd')
    // let history = useHistory();
    const [loggedIn, setloggedIn] = useState(false);
    useEffect(() => {
      let token = localStorage.getItem("token");
      if (token){
        // alert("!!")
        history.push("/dashboard");
      }else{
        history.push("/login");
      }
    }, [])

  return (
    <Loader
         type="Puff"
         color="#00BFFF"
         height={100}
         width={100}
         // timeout={3000}
      />
  );
}
  
const SampleComponent = () => {

return (
  <div>
    <Router>
      {Routes()}
      {/*<Loader/>*/}
      </Router>
  </div>
  )
}

export default SampleComponent;
